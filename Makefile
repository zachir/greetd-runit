all: greetd/run greetd/log/run
	chmod +x greetd/run greetd/log/run

install: all
	cp -rv greetd /etc/runit/sv/
	cp config.toml /etc/greetd/config.toml
